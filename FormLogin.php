<?php
session_start();
 if(isset($_SESSION['username'])){
    header('location:Dina\'shome.php');
}

?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> 
    <title>FormLoginDina</title>
    <style type="text/css">
      body{
        background-color:#ff7d69;
        background-size: cover;
        background-position: center;
        float: none;
        margin: 150px auto;
        margin-left: 40%; 
      }
      card-link {
        color: #000000
      }
    </style>
  <body>
    <div class="card border" style="max-width: 18rem;">
    <div class="card-header">
    <div class="card-body">
    <h5 class="card-title">Let's Login!</h5>
    <?php 

      if (isset($_GET["pesan"])){
      $pesan = $_GET["pesan"];
      }
      else {
      $pesan = "Please Login!";
      }
      
    ?>
    <div class="pesan" style="text-align: center; color: blue;">
      <?php echo $pesan;?>
    </div>
      <form method="POST" action="login_process.php">
       <div class="mb-3">
         <label class="form-label">Username</label>
         <input type="text" name="username" class="form-control" aria-describedby="emailHelp">
       </div>
       <div class="mb-3">
         <label for="exampleInputPassword1" class="form-label">Password</label>
         <input type="Password" name="password" class="form-control" id="exampleInputPassword1">
       </div>           
        <button type="submit" class="btn btn-primary">Login</button>
        <br>
        <br>
        Doesn't have an account? <a href="formregister.php" class="card-link"> Register here!</a>
      </form>

    </div>
  </body>
</html>