<?php
session_start();

Include 'koneksi.php';


  if (!isset($_SESSION['login'])){
    header('location:FormLogin.php');
  }


if (!isset($_GET=["pesan"])){

  if ($_GET=["pesan"] === "berhasil_hapus"){
  $pesan =  "Berhasil Menghapus Data";
  $warna =  "success";
  }
  if ($_GET=["pesan"] === "berhasil_ubah"){
  $pesan =  "Berhasil Mengubah Data";
  $warna =  "success";
  }
  if ($_GET=["pesan"] === "gagal_hapus"){
  $pesan =  "Gagal Menghapus Data";
  $warna = "danger";
}

}


$username = $_SESSION['username'];

$sql = "select * from user";
$result = mysqli_query($con, $sql);

// Fetch all
$hasil =  mysqli_fetch_all($result, MYSQLI_ASSOC);
 
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">`
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style >
      body {
        background-color: #ff7d69;
      }
      table {
        background-color: white;
      }
      h1 {
        font-family: snell;
      }
    </style>
  <body>
  <nav class="bg-light nav">
  <a class="nav-link active " aria-current="page" href="#">Home</a>
  <a class="nav-link" href="logout.php">Logout</a>
</nav>
<div class="container">
    <h1 style="margin-top : 100px !important; text-align : center; text-transform: uppercase;"> Welcome <?php echo $_SESSION['username']; ?></h1>
      <div class="d-flex" justify-content=end>
        <h3>Data User</h3><br>
        <a  class="btn btn-primary" href="formregister.php">Add Data +</a>
      </div>
       <?php
       if (isset($pesan)){

      ?>
  
       <div class="alert alert-<? = $warna;  ?>" role =  "alert" > 
               <? = $pesan; ?>
        </div>
        <?php
         }
        ?>
        <table class="table table-striped" border="1">
          <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Avatar</th>
            <th>Action</th>
          </tr>
          <tbody>
            <?php
            foreach ($hasil as $key => $user_data) {
            ?>
            <tr>
                <td><?=   $key + 1   ?> </td>
                <td><?=   $user_data["name"] ?></td>
                <td><?=   $user_data["username"] ?></td>     
                <td><?=   $user_data["email"] ?></td>
                <td><?=   $user_data["avatar"] ?></td>
                <td>
                  <a class="btn btn-success" href="edit_process.php?id=<?= 
                   $user_data['id']?>">Edit</a>
                  <a class="btn btn-danger" href="delete_process.php?id?= 
                   $user_data['id']?>">Delete</a>
                </td>
            </tr>
              <?php
              }

              ?>
          </tbody>
        </table>
</div>
<div class="footer">
  <footer>
    <div class="container">
      <span class="text-muted">cr : Athana.cp</span>
    </div>
  </footer>
  </body>
</html>