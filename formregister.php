<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> 
    <title>FormLoginDina</title>
    <style type="text/css">
      body{
        background-color:#ff7d69;
        background-size: cover;
        background-position: center;
        float: none;
        margin: 70px auto;
        margin-left: 40%; 
      }
      card-link {
        color: #000000
      }
    </style>
  <body>
    <div class="card border" style="max-width: 18rem;">
    <div class="card-header">
    <div class="card-body">
    <h5 class="card-title">Let's Register!</h5>
    <?php 

      if (isset($_GET["pesan"])){
      $pesan = $_GET["pesan"];
      }
      else {
      $pesan = "Please fill the data!";
      }

    ?>
    <div class="pesan" style="text-align: center; color: blue;">
      <?php echo $pesan;?>
    </div>
      <form method="POST" action="register_process.php">
       <div class="mb-3">
         <label class="form-label">Name</label>
         <input type="text" name="name" class="form-control">
       </div>
       <div class="mb-3">
         <label class="form-label">Username</label>
         <input type="text" name="username" class="form-control">
       </div>   
       <div class="mb-3">
         <label class="form-label">Email</label>
         <input type="email" name="email" class="form-control">
       </div>
       <div class="mb-3">
         <label class="form-label">Password</label>
         <input type="Password" name="password" class="form-control">
       </div>
       <div class="mb-3">
         <label class="form-label">Confirm Password</label>
         <input type="password" name="pass" class="form-control">
       </div> 
        <button type="submit" class= "btn btn-primary">submit</button>       
      </form>

    </div>
  </body>
</html>